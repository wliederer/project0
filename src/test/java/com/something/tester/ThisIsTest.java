package com.something.tester;

import static org.mockito.Mockito.*;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;


import com.something.collections.PasswordLedger;
import com.something.collections.TheLot;
import com.something.collections.User;
import com.something.serialize.Cereal;
import com.something.something.Car;
import com.something.something.Customer;
import com.something.something.Employee;
import com.something.something.Loan;
import com.something.something.Person;


import org.apache.log4j.Logger;
import static org.junit.Assert.assertEquals;

import java.util.HashMap;


public class ThisIsTest {
	
	
	
	//final static Logger logger = Logger.getLogger(User.class);
	 
	//static User cal;
	static PasswordLedger cal2 = new PasswordLedger();
	static TheLot cal3 = new TheLot();
	//Person tempPerson = new Person("Raisin","Bran");
	static User cal=new User();
	// HashMap<Integer,Person> testMap = new HashMap<>();
	 static Customer customer1 = new Customer(0,"billy","Jean","Customer","bill","bill");
	static Employee employee1 = new Employee(0,"Boss","man","Employee","admin","admin");
	static Car car1 = new Car(1, "hummer","h2",2003,2000, null);

	static Car car2 = new Car(2, "chrysler","lebaron",1996,2000, null);

	
	
	@BeforeClass
	public static void beforeAll() {
		car1.getOffers().put(1, 2000.00);
		 
		 
		 cal2.getLoggers().put(customer1.getUsername(), customer1.getPassword());
		 cal2.getLoggers().put(employee1.getUsername(), employee1.getPassword());
		 System.out.println(customer1.getTitle());
		 

		 
		//PasswordLedger cal2 = new PasswordLedger();
		//TheLot cal3 = new TheLot();
		System.out.println("=======before all=========");
	
	}
	@AfterClass
	public static void afterAll() {
		System.out.println("======after all=========");
	
	}
	@Before
	public void beforeTest() {
		System.out.println("==========before test=========");
		
	}
	@After
	public void afterTest() {
		System.out.println("==========after test=========");
	}
	@Test
	public void testStore() {
		customer1.setId(cal.getUsers().size()+1);
		cal.getUsers().put(customer1.getId(),customer1);
		employee1.setId(cal.getUsers().size()+1);
		cal.getUsers().put(employee1.getId(),employee1);
		assertEquals("test",2,cal.getUsers().size());
	}
	@Test
	public void testReturnId() {
		assertEquals(2,cal.returnId("admin",cal.getUsers()));
		
	}
	@Test
	public void testTitle() {
		HashMap<Integer,Person>users = cal.getUsers();
		String title = "Customer";
		
		assertEquals(title,cal.checkTitle("bill",users));
		
	}
	@Test
	public void testGetNextUserId() {
		assertEquals(3,cal.getNextUsersID());
	}
	
	@Test
	public void testLogin() {
		System.out.println("here");
		assertEquals("access granted",cal2.checkLogin("bill", "bill"));
	}
	
	@Test
	public void testPayments() {
		
		int key = 1;
		
		
		double price = car1.getOffers().get(key);
		System.out.println(price);
		
		double money = 3000.00;
		
		double principle = price - money;
		if(principle < 0) {
			System.out.println("straight cash baby");
			
		}
		else {
		double x = 2.52;//12 month loan
		double autoPayment = Math.round(principle*((.07*x)/(x-1)*100)/100);
		System.out.println(autoPayment);
		
		Loan newLoan = new Loan(autoPayment,12);
		System.out.println(newLoan);
		customer1.addToPayments(newLoan);
		
		assertEquals(0,customer1.getPayments().get(1));
	}
	
	
	}
}
