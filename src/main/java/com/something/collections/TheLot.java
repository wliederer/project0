package com.something.collections;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import com.something.something.Car;

public class TheLot implements Serializable{
/**
	 * 
	 */
	private static final long serialVersionUID = 1326386116623431308L;
	private HashMap<Integer, Car> cars  = new HashMap<>();
	int count = 0;
	
	
	
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public TheLot() {
		
	}
	public HashMap<Integer, Car> getCars() {
		return cars;
	}

	public void setCars(HashMap<Integer, Car> cars) {
		this.cars = cars;
	}
	//gets next car Id
//	public int getNextCarID() {
//		return cars.size() + 1;
//	}
	//sets car Id and stores it
	public void store(Car newCar) {
		newCar.setId(count);
		cars.put(newCar.getId(), newCar);
		count++;
	}
	//returns a list of car values---may delete
	public List<Car> getAllcars() {
		return new ArrayList<Car>(cars.values());

	}
	//prints all offers--------may delete
	public void printAllOffers() {
		Iterator<Integer> keySetIterator = cars.keySet().iterator();
		while(keySetIterator.hasNext()) {
			Integer key = keySetIterator.next();
			System.out.println("car ID: " +"\u001B[35m"+ key +"\u001B[0m"+ " offer: " +"\u001B[35m"+ cars.get(key).getOffers());
		}
	}
	//declines an offer----kinda useless but is being used
	public void declineOffer(Integer key, Double money) {
			cars.get(key).setSaleStatus("recently rejected offer " + money);
			System.out.println("offer "+money+" rejected for"+ cars.get(key));
			}
	//accepts an offer---kinda useless but is being used
	public void acceptOffer(Integer key, Double money) {
			cars.get(key).setSaleStatus("Sold offer accepted for " + money);
			cars.get(key).getOffers().clear();
			System.out.println("offer "+money+" accepted for"+ cars.get(key));
	}

}
