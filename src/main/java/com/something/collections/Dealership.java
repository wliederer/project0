package com.something.collections;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import com.something.db.ConnectionUtil;
import com.something.something.Car;
import com.something.something.Customer;
import com.something.something.Employee;
import com.something.something.Loan;
import com.something.something.Person;
import com.something.something.Scanhere;



public class Dealership {

	//retrieves users
	public HashMap<Integer,Person> pullUsers() {
		try{
			Connection conn = ConnectionUtil.connect();
			String sql = "select * from \"users\"";
			PreparedStatement s = conn.prepareStatement(sql);

			ResultSet rs = s.executeQuery();
			HashMap<Integer,Person> usersql = new HashMap<>();
			while(rs.next()) {
				if(rs.getString("title").equals("Employee")) {
					usersql.put(rs.getInt(1), new Employee(
							rs.getInt(1),
							rs.getString("firstname"),
							rs.getString("lastname"),
							rs.getString("title"),
							rs.getString("username"),
							rs.getString("password")
							));
				}else if(rs.getString("title").equals("Customer")) {
					usersql.put(rs.getInt(1), new Customer(
							rs.getInt(1),
							rs.getString("firstname"),
							rs.getString("lastname"),
							rs.getString("title"),
							rs.getString("username"),
							rs.getString("password")
							));
				}
			}	
			s.closeOnCompletion();
			conn.close();
			return usersql;
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	//push all users to sql
	public void pushUsers(HashMap<Integer,Person> map) {
		int id=0;
		String fname="";
		String lname="";
		String title="";
		String uname="";
		String pword="";
		try{
			Connection conn = ConnectionUtil.connect();
			String sql = "insert into \"users\" values('"+id+"', '"+fname+"', '"+lname+"', '"+title+
					"', '"+uname+"', '"+pword+ "') on conflict do nothing";
			Iterator<HashMap.Entry<Integer,Person>> itr = map.entrySet().iterator();
			while(itr.hasNext()) {
				HashMap.Entry<Integer,Person> entry = itr.next();
				fname=entry.getValue().getFirstName();
				lname=entry.getValue().getLastName();
				title=entry.getValue().getTitle();
				uname=entry.getValue().getUsername();
				pword=entry.getValue().getPassword();
				id=entry.getValue().getId();

				PreparedStatement s = conn.prepareStatement(sql);
				s.executeUpdate();
				s.closeOnCompletion();//double check
				conn.close();
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}
	}
	//insert user
	public void insertUsers(Person person) {
		int id=person.getId();
		String fname=person.getFirstName();
		String lname=person.getLastName();
		String title=person.getTitle();
		String uname=person.getUsername();
		String pword=person.getPassword();
		try{
			Connection conn = ConnectionUtil.connect();
			String sql = "insert into \"users\" values('"+id+"', '"+fname+"', '"+lname+"', '"+title+
					"', '"+uname+"', '"+pword+ "') on conflict do nothing";

			PreparedStatement s = conn.prepareStatement(sql);
			s.executeUpdate();
			s.closeOnCompletion();//double check
			conn.close();

		}catch(SQLException e) {
			e.printStackTrace();
		}
	}
	//pull all paswords from sql
	public HashMap<String,String> pullLedger() {
		try{
			Connection conn = ConnectionUtil.connect();
			String sql = "select * from \"passwordledger\"";
			PreparedStatement s = conn.prepareStatement(sql);
			HashMap<String,String> wordLedger = new HashMap<>();
			ResultSet rs = s.executeQuery();

			while(rs.next()) {
				wordLedger.put(rs.getString("username"), rs.getString("password")

						);
			}	
			s.closeOnCompletion();
			conn.close();
			return wordLedger;

		}catch(SQLException e) {
			e.printStackTrace();
		}
		return null;

	}
	//push all paswords to sql
	public void pushLedger(HashMap<String,String> map) {
		String uname="";
		String pword="";
		try{
			Connection conn = ConnectionUtil.connect();
			String sql = "insert into \"passwordledger\" values('"+uname+"', '"+pword+"') on conflict do nothing";
			Iterator<HashMap.Entry<String,String>> itr = map.entrySet().iterator();
			while(itr.hasNext()) {
				HashMap.Entry<String, String> entry = itr.next();

				uname=entry.getValue();
				pword=entry.getValue();

				PreparedStatement s = conn.prepareStatement(sql);
				s.executeUpdate();
				s.closeOnCompletion();
				conn.close();
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}
	}
	//insert password
	public void insertLedger(Person person) {

		String uname=person.getUsername();
		String pword=person.getPassword();
		try{
			Connection conn = ConnectionUtil.connect();
			String sql = "insert into \"passwordledger\" values('"+uname+"', '"+pword+"') on conflict do nothing";

			PreparedStatement s = conn.prepareStatement(sql);
			s.executeUpdate();
			s.closeOnCompletion();//double check
			conn.close();

		}catch(SQLException e) {
			e.printStackTrace();
		}
	}
	//pull call cars from sql
	public HashMap<Integer,Car> pullCars() {
		try{
			Connection conn = ConnectionUtil.connect();
			String sql = "select * from \"cars\"";
			PreparedStatement s = conn.prepareStatement(sql);
			HashMap<Integer,Car> cars = new HashMap<>();
			ResultSet rs = s.executeQuery();

			while(rs.next()) {

				cars.put(rs.getInt(1), new Car(
						rs.getInt(1),
						rs.getString("make"),
						rs.getString("model"),
						rs.getInt("year"),
						rs.getDouble("price"),
						rs.getString("salestatus")
						));
			}	
			s.closeOnCompletion();
			conn.close();
			return cars;

		}catch(SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	//push all cars to sql
	public void pushCars(HashMap<Integer,Car> map) {
		int id=0;
		String make="";
		String model="";
		int year=0;
		double price=0;
		String salestatus="";
		try{
			Connection conn = ConnectionUtil.connect();
			String sql = "insert into \"users\" values('"+id+"', '"+make+"', '"+model+"', '"+year+
					"', '"+price+"', '"+salestatus+ "')";
			Iterator<HashMap.Entry<Integer,Car>> itr = map.entrySet().iterator();
			while(itr.hasNext()) {
				HashMap.Entry<Integer,Car> entry = itr.next();
				make=entry.getValue().getMake();
				model=entry.getValue().getModel();
				year=entry.getValue().getYear();
				price=entry.getValue().getPrice();
				salestatus=entry.getValue().getSaleStatus();
				id=entry.getValue().getId();

				PreparedStatement s = conn.prepareStatement(sql);

				s.executeUpdate();
				s.closeOnCompletion();
				conn.close();
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}
	}
	//insert car
	public void insertCar(Car car) {
		int id = car.getId();
		String make=car.getMake();
		String model=car.getModel();
		int year = car.getYear();
		double price = car.getPrice();
		String status = car.getSaleStatus();

		try{
			Connection conn = ConnectionUtil.connect();
			String sql = "insert into \"cars\" values('"+id+"','"+make+"', '"+model+"','"+year+"','"+price+"','"+status+"')";
		//	"insert into \"cars\"(make,model,year,price,salestatus) values('"+make+"', '"+model+"','"+year+"','"+price+"','"+status+"')";
			PreparedStatement s = conn.prepareStatement(sql);
			s.executeUpdate();
			s.closeOnCompletion();//double check
			conn.close();

		}catch(SQLException e) {
			e.printStackTrace();
		}
	}
	//return total car count
	public int countCar() {
		
		try{
			Connection conn = ConnectionUtil.connect();
			String sql = "{?= call get_largest() }";

			CallableStatement s = conn.prepareCall(sql);
			s.registerOutParameter(1, Types.INTEGER);
			s.execute();
			int results = s.getInt(1);
			
			s.closeOnCompletion();//double check
			conn.close();
			return results;
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return -1;
	}
	//delete car
	public void deleteCar(int carid) {

		int id=carid;

		try{
			Connection conn = ConnectionUtil.connect();
			String sql = "delete from \"cars\" where \"carid\" = '"+id+"'";

			PreparedStatement s = conn.prepareStatement(sql);
			s.executeUpdate();
			s.closeOnCompletion();//double check
			conn.close();

		}catch(SQLException e) {
			e.printStackTrace();
		}
	}
	//insert offer
	public void insertOffer(int carId, int userId, double offer) {

		int carid = carId;
		int userid = userId;
		double money = offer;


		try{
			Connection conn = ConnectionUtil.connect();
			String sql ="insert into \"offers\" values('"+carid+"', '"+userid+"','"+money+"')";

			PreparedStatement s = conn.prepareStatement(sql);
			s.executeUpdate();
			s.closeOnCompletion();//double check
			conn.close();

		}catch(SQLException e) {
			e.printStackTrace();
		}
	}
	//delete specific offer
		public void deleteOffer(int carId, int userId, double offer) {

			int carid = carId;
			int userid = userId;
			double money = offer;


			try{
				Connection conn = ConnectionUtil.connect();
				String sql =  "delete from \"offers\" where \"carid\"= '"+carid+"' and \"customer\" = '"+userid+"'and \"offer\" ='"+money+"'";

				PreparedStatement s = conn.prepareStatement(sql);
				s.executeUpdate();
				s.closeOnCompletion();//double check
				conn.close();

			}catch(SQLException e) {
				e.printStackTrace();
			}
		}
		//delete specific offer
				public void deleteAllOffer(int carId) {

					int carid = carId;
					


					try{
						Connection conn = ConnectionUtil.connect();
						String sql =  "delete from \"offers\" where \"carid\"= '"+carid+"'";

						PreparedStatement s = conn.prepareStatement(sql);
						s.executeUpdate();
						s.closeOnCompletion();//double check
						conn.close();

					}catch(SQLException e) {
						e.printStackTrace();
					}
				}
		//retrieves offers
		public void pullOffers() {
			try{
				Connection conn = ConnectionUtil.connect();
				String sql = "select * from \"offers\"";
				PreparedStatement s = conn.prepareStatement(sql);

				ResultSet rs = s.executeQuery();
				//HashMap<Integer,Double> useroffers = new HashMap<>();
				while(rs.next()) {
						Scanhere.fastCars.getCars().get(rs.getInt(1)).getOffers().put(rs.getInt("customer"), rs.getDouble("offer"));
					
				}	
				s.closeOnCompletion();
				conn.close();
			
			}catch(SQLException e) {
				e.printStackTrace();
			}
		}
		//retrieves garages
				public void pullgarage() {
					try{
						Connection conn = ConnectionUtil.connect();
						String sql = "select * from \"garages\"";
						PreparedStatement s = conn.prepareStatement(sql);

						ResultSet rs = s.executeQuery();
						//HashMap<Integer,Double> useroffers = new HashMap<>();
						while(rs.next()) {
								Customer user = (Customer) Scanhere.allUsers.getUsers().get(rs.getInt("customer"));
								
								user.getGarage().put(user.getGarage().size()+1,new Car(rs.getString("make"),rs.getString("model"),rs.getInt("year")));
							
						}	
						s.closeOnCompletion();
						conn.close();
					
					}catch(SQLException e) {
						e.printStackTrace();
					}
				}
	//insert garage
	public void insertGarage(int carid, int custid, String make, String model, int year) {


		try{
			Connection conn = ConnectionUtil.connect();
			//String sql = "insert into \"garages\" values('"+carid+"', '"+custid+"','"+make+"','"+model+"','"+year+"')";
			String sql = "insert into \"garages\" values(?,?,?,?,?)";
			
			PreparedStatement s = conn.prepareStatement(sql);
			s.setInt(1, carid);
			s.setInt(2, custid);
			s.setString(3, make);
			s.setString(4, model);
			s.setInt(5,year);
			
			s.executeUpdate();
			s.closeOnCompletion();//double check
			conn.close();

		}catch(SQLException e) {
			e.printStackTrace();
		}
	}
	//insert garage
		public void insertPayment(int custid, double pay, int num) {


			try{
				Connection conn = ConnectionUtil.connect();
				String sql = "insert into \"payment\" values('"+custid+"', '"+pay+"','"+num+"')";

				PreparedStatement s = conn.prepareStatement(sql);
				s.executeUpdate();
				s.closeOnCompletion();//double check
				conn.close();

			}catch(SQLException e) {
				e.printStackTrace();
			}
		}
		//retrieves payments
		public void pullPayment() {
			try{
				Connection conn = ConnectionUtil.connect();
				String sql = "select * from \"payment\"";
				PreparedStatement s = conn.prepareStatement(sql);

				ResultSet rs = s.executeQuery();
				//HashMap<Integer,Double> useroffers = new HashMap<>();
				while(rs.next()) {
						Customer user = (Customer) Scanhere.allUsers.getUsers().get(rs.getInt("customer"));
						
						user.getPayments().put(user.getPayments().size()+1,new Loan(rs.getDouble("payment"),rs.getInt("numof")));
					
				}	
				s.closeOnCompletion();
				conn.close();
			
			}catch(SQLException e) {
				e.printStackTrace();
			}
		}
}
