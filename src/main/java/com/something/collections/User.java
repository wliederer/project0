package com.something.collections;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.something.something.Car;
import com.something.something.Customer;
import com.something.something.Person;
import com.something.something.Scanhere;

public class User {
	protected HashMap<Integer, Person> users  = new HashMap<>();

	

	public User() {

	}

	public HashMap<Integer, Person> getUsers() {
		return users;
	}

	public void setUsers(HashMap<Integer, Person> users) {
		this.users = users;
	}
	//stores person into user map, 
	public void store(Person person) {
		person.setId(getNextUsersID());
		users.put(person.getId(), person);
		System.out.println("test");
	}
	//used to set currentGlobal
	public int returnId(String user, HashMap<Integer,Person> users) {
		int currentId=0;
		for(Person search : users.values()) {
			if(search.getUsername().equals(user)) {
				currentId = search.getId();
			}
		}
		return currentId;
	}
	//Used to verify employee or customer
	public String checkTitle(String user, HashMap<Integer,Person> users) {
		String title ="";
		for(Person search : users.values()) {
			if(search.getUsername().equals(user) && search.getTitle().equals("Employee")) {
				title = "Employee";
				System.out.println("User is a Employee");
				//Scanhere.employeeLot();

			}else if(search.getUsername().equals(user) && search.getTitle().equals("Customer")) {
				title = "Customer";
				System.out.println("User is a Customer");
				//Scanhere.customerLot();
			}	
		}
		return title;
		
	}
	//set users ID based on size of map
	public int getNextUsersID() {
		return users.size() + 1;
	}
	//list all user values------------may delete
	public List<Person> getAllUsers() {
		return new ArrayList<>(users.values());

	}
	//returns all customers in list------------may delete
	public List<Customer> getAllCustomers(){
		List<Customer> customers = new ArrayList<>();
		for(Integer num : users.keySet()) {
			String who = users.get(num).getTitle();
			if(who.equals("Customer")) {
				customers.add((Customer) users.get(num));
			}
		}
		return customers;
	}
	//returns List of customer garages-----------may delete
//	public List<HashMap<Integer,Car>> getAllGarage(){
//		List<Customer> customers = User.getInstance().getAllCustomers();
//		List<HashMap<Integer,Car>> garages =  new ArrayList<>();
//		Iterator<Customer> it = customers.iterator();
//		while (it.hasNext()) {
//			garages.add(it.next().getGarage());
//		}
//		return garages;
//	}
	//removes offers from non accepted users
//	public void removeOffers(Double userId) {
//		for (HashMap<Integer, Car> hashMap : User.getInstance().getAllGarage()){
//			for (Map.Entry<Integer, Car> entry  : hashMap.entrySet()){
//			
//				System.out.println(entry.getValue());
//				System.out.println(entry.getKey());
//				if(entry.getValue()!=userId) {
//					//System.out.println("inside if");
//					int i = entry.getKey();
//					hashMap.remove(i);
//				}
//			}
//		}
//	}



}
