package com.something.collections;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.something.something.Person;



public class PasswordLedger {
	protected HashMap<String, String> loggers  = new HashMap<>();
	
	
	public PasswordLedger() {
		
	}

	public HashMap<String, String> getLoggers() {
		return loggers;
	}

	public void setLoggers(HashMap<String, String> loggers) {
		this.loggers = loggers;
	}
	//verifys login
	public String checkLogin(String string1,String string2) {
		String string = "check";
		if(loggers.containsKey(string1)&&loggers.get(string1).equals(string2)) {
			string = "access granted";
			System.out.println(string);
		}else
			string = "username or password incorrect";
			System.out.println(string);
		return string;
	
	}
	//returns passwords not even using
	public List<String> getAllPasswords() {
		return new ArrayList<>(loggers.values());
	
	}
	//stores a person into the password ledger probably repetitive and useless
	public void store(Person person) {
		loggers.put(person.getUsername(), person.getPassword());
	
	}
	
}
