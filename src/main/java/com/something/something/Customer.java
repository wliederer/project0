package com.something.something;


import java.util.HashMap;

import com.something.collections.TheLot;
import com.something.collections.User;


public class Customer extends Person {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8335888845692520599L;
	private HashMap<Integer,Loan> payments = new HashMap<>();
	private HashMap<Integer,Car> garage = new HashMap<>();

	
	public Customer(int id, String firstName, String lastName, String title, String username, String password) {
		super(id, firstName, lastName, title, username, password);
		// TODO Auto-generated constructor stub
		title = "Customer";
		id = 0;
	}

	public Customer() {
		super();
		title = "Customer";
		id = 0;
	}
	
	public HashMap<Integer, Loan> getPayments() {
		return payments;
	}
	public void setPayments(HashMap<Integer, Loan> payments) {
		this.payments = payments;
	}
	@Override
	public String toString() {
		return " ["+"\u001B[35m"+"firstName="+firstName+"\u001B[0m"+", lastName="+lastName+", Title="+title+ "\u001B[35m"+", id="+id+"\u001B[0m"+ ", payment=" + payments + "]";
	}

	public HashMap<Integer, Car> getGarage() {
		return garage;
	}
	public void setGarage(HashMap<Integer, Car> garage) {
		this.garage = garage;
	}
//adds to customer garage---its probly repetitive of the put method from maps
	public void addToGarage(Car car) {
		garage.put(garage.size()+1, car);

	}
	//add payment to payments with id that will match new car
	public void addToPayments(Loan loan) {
		payments.put(payments.size()+1, loan);

	}
	

	//prints customers garage----may delete
	public void printCustomerGarage(){
		for (Object key : garage.keySet()) {
			Car car = garage.get(key);
			System.out.println("\u001B[35m"+"Your car = make: " + car.getMake()+"\u001B[0m"+" , model: "+car.getModel()+" , year: "+car.getYear());
		}
	}
	//calculate monthly payment
	public double payments(double money, int carId, int userId) {
		double autoPayment=0;
		
		Customer current = (Customer) Scanhere.allUsers.getUsers().get(userId);
		double downPayment = money;
		double price = Scanhere.fastCars.getCars().get(carId).getPrice();
		double principle = price - downPayment;
		if(principle < 0) {
			System.out.println("straight cash baby");
			return 0;
		}
		else {
		double x = 2.52;//12 month loan
		autoPayment = Math.round(principle*((.07*x)/(x-1)*100)/100);
		
		System.out.println(current.getFirstName()+" has a new payment of"+"\u001B[35m"+" $"+autoPayment+" per month for 12 months");
		return autoPayment;
		}
	}




}
