package com.something.something;


import java.io.Serializable;

public class Person implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4927058516923888397L;
	protected String firstName;
	protected String lastName;
	protected String title;
	protected int id;
	protected String username;
	protected String password;
	
	

	public Person(int id, String firstName, String lastName,String title ,String username, String password) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.title = title;
		this.id = id;
		this.username = username;
		this.password = password;
	}
	
	public Person() {
		
	}
	
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public String getUsername() {
		return username;
	}


	public void setUsername(String username) {
		this.username = username;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	@Override
	public String toString() {
		return " ["+"\u001B[35m"+"firstName=" + firstName+"\u001B[0m" + ", lastName=" + lastName + ", title=" + title +"\u001B[35m"+ ", id=" + id
				+"\u001B[0m"+ "]";
	}
	
	
	

	
}
