package com.something.something;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;


public class Car implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8402864913613641591L;
	private String make;
	private String model;
	private int year;
	private double price;
	private int id;
	private HashMap<Integer,Double> offers = new HashMap<Integer,Double>();
	private String saleStatus;
	
	
	public Car(int id, String make, String model, int year, double price, String saleStatus) {
		super();
		this.make = make;
		this.model = model;
		this.year = year;
		this.price = price;
		this.id=id;
		this.saleStatus=saleStatus;
	}
	public Car(String make, String model, int year) {
		super();
		this.make=make;
		this.model=model;
		this.year=year;
	}
	public Car() {
		
	}
	public String getMake() {
		return make;
	}
	public void setMake(String make) {
		this.make = make;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	
	public HashMap<Integer, Double> getOffers() {
		return offers;
	}

	public void setOffers(HashMap<Integer, Double> offers) {
		this.offers = offers;
	}

	public void addOffer(int global, double money) {
		offers.put(global,money);
	}

	public String getSaleStatus() {
		return saleStatus;
	}

	public void setSaleStatus(String saleStatus) {
		this.saleStatus = saleStatus;
	}

	@Override
	public String toString() {
		return "Car [make=" + make + ", model=" + model + ", year=" + year + ", price=" + price + ","+"\u001B[35m"+ "id=" + id
				+", offers=" + offers + "\u001B[0m"+"]";
	}

	
}
