package com.something.something;

import java.io.Serializable;

public class Loan implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5037476385717230522L;
	private Double payment;
	private int numofPayments;
	
	
	public Loan(Double payment, int numofPayments) {
		super();
		this.payment = payment;
		this.numofPayments = numofPayments;
	}
	public Double getPayment() {
		return payment;
	}
	public void setPayment(Double payment) {
		this.payment = payment;
	}
	public int getNumofPayments() {
		return numofPayments;
	}
	public void setNumofPayments(int numofPayments) {
		this.numofPayments = numofPayments;
	}
	@Override
	public String toString() {
		return "Loan ["+"\u001B[35m"+"payment=" + payment +"\u001B[0m"+ ", numofPayments=" + numofPayments + "]";
	}
	
	
	
	
}
