package com.something.something;

import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import com.something.collections.Dealership;
import com.something.collections.PasswordLedger;
import com.something.collections.TheLot;
import com.something.collections.User;
import com.something.serialize.Cereal;

public class Scanhere {
	public static User allUsers = new User();
	public static PasswordLedger bigWords = new PasswordLedger();
	public static TheLot fastCars = new TheLot();
	public static Dealership deal = new Dealership();
	
	//logger log4j
	final static Logger logger = Logger.getLogger(Scanhere.class);


	static int currentGlobal;
	//Main Menu
	public static void mainMenu() {
		Scanner s = new Scanner(System.in);
//		String file = "./ObjectMapUserFile.txt";
//		String file2 ="./ObjectMapPasswordFile.txt";
//		String file3 ="./ObjectMapCarsFile.txt";
//		String file4="./ObjectLot.txt";

		System.out.println("pick an option");
		System.out.println("-----1 User Register");
		System.out.println("-----2 Login");
		System.out.println("-----3 Save**not being used cause db**");
		System.out.println("-----4 Load");
		System.out.println("-----5 End");


		String x = s.nextLine();
		switch(x) {
		case "1":
			System.out.println("Register");
			userRegister();
			break;
		case "2":
			System.out.println("Enter Username and Password");
			login();
			break;
		case "3":
			System.out.println("does nothing");

			//deal.pushUsers(allUsers.getUsers());
			//deal.pushLedger(bigWords.getLoggers());
//			Cereal.writeObjectMap(file, allUsers.getUsers());
//			Cereal.writeObject(file4, fastCars);
//			Cereal.writeObjectMap2(file2, bigWords.getLoggers());
//			Cereal.writeObjectMap(file3, fastCars.getCars());
			mainMenu();

			break;
		case "4":
			System.out.println("Load");
			allUsers.setUsers(deal.pullUsers());
			bigWords.setLoggers(deal.pullLedger());
			fastCars.setCars(deal.pullCars());
			fastCars.setCount(deal.countCar()+1);
			deal.pullOffers();
			deal.pullgarage();
			deal.pullPayment();
			//fastCars = (TheLot) Cereal.readObject(file4);
			//@SuppressWarnings("unchecked") 
			//HashMap<Integer,Person> users=(HashMap<Integer, Person>) Cereal.readObjectMap(file);
			//allUsers.setUsers(users);
//			@SuppressWarnings("unchecked") 
//			HashMap<String,String> loggers = (HashMap<String,String>) Cereal.readObjectMap2(file2);
//			bigWords.setLoggers(loggers);
//			@SuppressWarnings("unchecked") 
//			HashMap<Integer,Car> cars=(HashMap<Integer, Car>) Cereal.readObjectMap(file3);
//			fastCars.setCars(cars);
			mainMenu();

			break;
			
		case "5":
			System.out.println("The End");
			System.exit(0);
			break;
		default:
			System.out.println("Input not valid");
			mainMenu();
			break;
		}
		s.close();
	}
	//User Registration
	public static void userRegister() {
		Scanner s = new Scanner(System.in);
		System.out.println("pick an option");
		System.out.println("-----1 Register Customer");
		System.out.println("-----2 Register Employee");

		String x = s.nextLine();
		if(x.equals("1")) {
			//PropertyConfigurator.configure("log4j.properties");
			logger.info("new customer was registered");

			Customer newCustomer = new Customer();
			Person person = newCustomer;
			create(person);

		}else if(x.equals("2")) {
			logger.info("new employee was registered");

			Employee newEmployee = new Employee();
			Person person = newEmployee;
			create(person);
		}else
			System.out.println("Input not valid");
		userRegister();
		s.close();
	}

	//Method to create employee & customer
	public static void create(Person person) {
		logger.info("a person was created");
		Scanner s = new Scanner(System.in);

		System.out.print("Enter first name:");
		person.setFirstName(s.nextLine());

		System.out.print("Enter last name:");
		person.setLastName(s.nextLine());

		System.out.println("Enter Username");
		String uname = s.nextLine();
		if(bigWords.getLoggers().containsKey(uname)) {
			System.out.println("username exists already restart");
			userRegister();
		}else
			person.setUsername(uname);


		System.out.println("Enter a password dont forget it!");
		person.setPassword(s.nextLine());

		allUsers.store(person);
		bigWords.store(person);
		deal.insertUsers(person);
		deal.insertLedger(person);

		for (int counter = 0; counter < allUsers.getAllUsers().size(); counter++) { 		      
			System.out.println(allUsers.getAllUsers().get(counter)); 		
		}   	

		System.out.println("press any key to contiue back to main menu");
		s.nextLine();
		mainMenu();
		s.close();
	}

	//Login method to check user name/password and verify customer or employee
	public static void login() {
		Scanner s = new Scanner(System.in);

		System.out.println("Enter Username");
		String user = s.nextLine();
		System.out.println("Enter Password");
		String pass = s.nextLine();
		bigWords.checkLogin(user,pass);

		currentGlobal = allUsers.returnId(user, allUsers.getUsers());
	//	System.out.println("Current Global: "+ currentGlobal);

		String title =allUsers.checkTitle(user,allUsers.getUsers());

		if(title=="Employee") {

			employeeLot();

		}else if(title=="Customer") {

			customerLot();
		}else
			System.out.println("Username or password incorrect bye bye");
		//		s.nextLine();
		mainMenu();
		s.close();

	}

	//CarLot with employee actions
	public static void employeeLot() {
		Scanner s = new Scanner(System.in);

		System.out.println("pick an option");
		System.out.println("-----1 View Car to the Lot");
		System.out.println("-----2 add Car to the Lot");
		System.out.println("-----3 reject an offer");
		System.out.println("-----4 accept an offer");
		System.out.println("-----5 View Payments");
		System.out.println("-----6 Remove car");

		System.out.println("-----8 Return to main menu");

		String x = s.nextLine();
		if(x.equals("1")) {
			for (int counter = 0; counter < fastCars.getAllcars().size(); counter++) { 		      
				System.out.println(fastCars.getAllcars().get(counter)+"\n"); 		
			}   

			employeeLot();

		}else if(x.equals("2")) {
			logger.info("car was added to lot");

			Car newCar = new Car();
			addCar(newCar);
		}else if(x.equals("3")) {
			fastCars.printAllOffers();
			int carId =0;
			int userId=0;
			System.out.println("enter car Id to modify offer\n");
			try{
				carId = s.nextInt();
			}catch(Exception e) {
				System.out.println("not an int");
				employeeLot();
			}
			if(fastCars.getCars().containsKey(carId)) {
				System.out.println("enter offer id to reject\n");
				try{userId = s.nextInt();}
				catch(Exception e) {
					System.out.println("not an int");
					employeeLot();
				}if(fastCars.getCars().get(carId).getOffers().containsKey(userId)) {
					double money = fastCars.getCars().get(carId).getOffers().get(userId);

					fastCars.getCars().get(carId).getOffers().remove(userId);
					fastCars.declineOffer(carId, money);
					deal.deleteOffer(carId, userId, money);
					logger.info("offer declined");

					System.out.println(allUsers.getUsers().get(userId).getUsername()+" was rejected for"+ fastCars.getCars().get(carId));

					System.out.println("returning to employee lot menu");

					employeeLot();
				}else {
					System.out.println("car does not contain offer at that Id");
					employeeLot();
				}
			}else{
				System.out.println("car id not in system");
				employeeLot();
			}


		}else if(x.equals("4")) {
			fastCars.printAllOffers();
			int carId = 0;
			int userId = 0;
			double money = 0;
			System.out.println("\nenter car Id to modify offer");
			try { carId = s.nextInt();
			}catch(Exception e) {
				System.out.println("not int");
				employeeLot();
			}
			if(fastCars.getCars().containsKey(carId)) {
				System.out.println("\nenter offer Id to accept");
				try { userId = s.nextInt();
				}catch(Exception e) {
					System.out.println("not a doub");
					employeeLot();
				}
				if(fastCars.getCars().get(carId).getOffers().containsKey(userId)) {
					money = fastCars.getCars().get(carId).getOffers().get(userId);

					fastCars.acceptOffer(carId, money);
					deal.deleteAllOffer(carId);
					logger.info("offer was accepted");
					
					//calculate payment and add to customer
					Customer current = (Customer) allUsers.getUsers().get(userId);
					double autoPayment = current.payments(money, carId, userId);
					Loan newLoan = new Loan(autoPayment,12);
					current.addToPayments(newLoan);
					
					//add payment to table
					deal.insertPayment(current.getId(), autoPayment, 12);
					
					//place car in user garage 
					((Customer) allUsers.getUsers().get(userId)).addToGarage(fastCars.getCars().get(carId));
					deal.insertGarage(fastCars.getCars().get(carId).getId(),allUsers.getUsers().get(userId).getId(),fastCars.getCars().get(carId).getMake(),fastCars.getCars().get(carId).getModel(),fastCars.getCars().get(carId).getYear());
					System.out.println("New car placed in buyers garage "+((Customer) allUsers.getUsers().get(userId)).getGarage()+"\n");
					logger.info("a user gained a car");

					//Remove from the Lot
					System.out.println(fastCars.getCars().get(carId).getModel()+" removed from the lot");
					fastCars.getCars().remove(carId);
					deal.deleteCar(carId);
					logger.info("car removed from lot during purchase");

					System.out.println("returning to employee lot menu");

					employeeLot();
				}else {
					System.out.println("offerId not in car system");
					employeeLot();
				}
			}else {
				System.out.println("car id not in system");
				employeeLot();
			}


		}
		else if(x.equals("5")) {
			List<Customer> customers = allUsers.getAllCustomers();
			for (int i = 0; i < customers.size(); i++) {
				System.out.println("\u001B[95m"+customers.get(i).getUsername()+"\u001B[0m" + customers.get(i).getPayments()+"\n");
			}
			employeeLot();

		}else if(x.equals("6")) {
			System.out.println("enter car id to remove car");
			for (int counter = 0; counter < fastCars.getAllcars().size(); counter++) { 		      
				System.out.println(fastCars.getAllcars().get(counter)+"\n"); 		
			}   
			int carid=0;
			try{carid = s.nextInt();}
			catch(Exception e) {
				System.out.println("not an int");
				employeeLot();
			}
			if(fastCars.getCars().containsKey(carid)) {
				fastCars.getCars().remove(carid);
				deal.deleteCar(carid);
				deal.deleteAllOffer(carid);
				logger.info("car was removed");
				employeeLot();
			}else {
				System.out.println("car not in system");
				employeeLot();
			}


			employeeLot();
		}
		else if(x.equals("8")) {
			mainMenu();
		}
		else
			System.out.println("invalid option");
		employeeLot();

		s.close();
	}
	//CarLot with Customer actions
	public static void customerLot() {
		Scanner s = new Scanner(System.in);

		System.out.println("pick an option");
		System.out.println("-----1 View Car to the Lot");
		System.out.println("-----2 make Offers");
		System.out.println("-----3 get monthly Payments");
		System.out.println("-----4 view your garage");
		System.out.println("-----5 Return to main menu");

		String x = s.nextLine();
		if(x.equals("1")) {
			for (int counter = 0; counter < fastCars.getAllcars().size(); counter++) { 		      
				System.out.println(fastCars.getAllcars().get(counter)+"\n"); 		
			}   
			customerLot();

		}else if(x.equals("2")) {
			for (int counter = 0; counter < fastCars.getAllcars().size(); counter++) { 		      
				System.out.println(fastCars.getAllcars().get(counter)+"\n"); 		
			}  
			int carid =0;
			double offer = 0;
			System.out.println("Enter car Id you want to offer on");
			try { carid = s.nextInt();}
			catch(Exception e) {
				System.out.println("not an int");
				customerLot();
			}
			if(fastCars.getCars().containsKey(carid)) {
				//System.out.println("car existence checked");
				System.out.println("Enter your offer amount");
				try { offer = s.nextDouble();}
				catch(Exception e) {
					System.out.println("not an doub");
					customerLot();
				}

				//set offer to car
				if(fastCars.getCars().get(carid).getOffers().containsKey(currentGlobal)) {
					System.out.println("you already have offer on car");
					customerLot();
				}else {
					fastCars.getCars().get(carid).addOffer(currentGlobal, offer);
					deal.insertOffer(carid, currentGlobal, offer);
					System.out.println("offer added to: "+fastCars.getCars().get(carid));
					logger.info("and offer was made");
				}
				s.nextLine();
				customerLot();

			}else 
				System.out.println("car id does not exist");
			customerLot();




		}else if(x.equals("3")) {

			System.out.println(allUsers.getUsers().get(currentGlobal).getFirstName() + ((Customer) allUsers.getUsers().get(currentGlobal)).getPayments());

			customerLot();

		}else if(x.equals("4")) {

			((Customer)allUsers.getUsers().get(currentGlobal)).printCustomerGarage();
			customerLot();

		}else if(x.equals("5")) {
			mainMenu();
		}else
			System.out.println("not valid input");
			customerLot();

		s.close();
	}


	//Method to add car to the lot
	public static void addCar(Car newCar) {
		Scanner s = new Scanner(System.in);

		System.out.println("Enter Make");
		newCar.setMake(s.nextLine());

		System.out.println("Enter Model");
		newCar.setModel(s.nextLine());

		System.out.println("Enter Year");
		try {
			newCar.setYear(s.nextInt());
		}catch(Exception e) {
			System.out.println("was not int, back to lot!");
			employeeLot();
		}

		System.out.println("Enter Price");
		try {
			newCar.setPrice(s.nextDouble());
		}catch(Exception e) {
			System.out.println("was not a doub, back to lot!");
		}

		fastCars.store(newCar);
		deal.insertCar(newCar);
		System.out.println("new car added");
		logger.info("car added to lot");

		//System.out.println(fastCars.getAllcars());

		System.out.println("Press any key to return to employee options");

		s.nextLine();
		employeeLot();
		s.close();
	}

}
