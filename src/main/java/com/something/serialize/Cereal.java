package com.something.serialize;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.List;

import com.something.something.Person;



public class Cereal {
	
	public static Object readObject(String filename) {
		try(ObjectInputStream ois = new ObjectInputStream(
				new FileInputStream(filename))){
			Object obj = ois.readObject();
			System.out.println(obj);
			return obj;
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	public static void writeObject(String filename, Object o) {
		try(ObjectOutputStream oos = new ObjectOutputStream(
				new FileOutputStream(filename))){
			
			oos.writeObject(o);
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("unchecked")
	public static Object readObjectList(String filename) {
		try(ObjectInputStream ois = new ObjectInputStream(
				new FileInputStream(filename))){
			List<Person> obj = (List<Person>)ois.readObject();
			System.out.println(obj);
			return obj;
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	public static void writeObjectList(String filename, List<Person> o) {
		try(ObjectOutputStream oos = new ObjectOutputStream(
				new FileOutputStream(filename))){
			
			oos.writeObject(o);
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	//map object for <Integer,Person>
	public static Object readObjectMap(String filename) {
		try(ObjectInputStream ois = new ObjectInputStream(
				new FileInputStream(filename))){
			@SuppressWarnings("unchecked")
			HashMap<Integer,Object> obj = (HashMap<Integer,Object>)ois.readObject();
			System.out.println(obj);
			return obj;
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	public static void writeObjectMap(String filename, Object object) {
		try(ObjectOutputStream oos = new ObjectOutputStream(
				new FileOutputStream(filename))){
			
			oos.writeObject(object);
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	//map object for <Integer,Car>
	/*
		public static Object readObjectMap3(String filename) {
			try(ObjectInputStream ois = new ObjectInputStream(
					new FileInputStream(filename))){
				@SuppressWarnings("unchecked")
				HashMap<Integer,Car> obj = (HashMap<Integer,Car>)ois.readObject();
				System.out.println(obj);
				return obj;
				
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			}
		}
		
		public static void writeObjectMap3(String filename, Object Car) {
			try(ObjectOutputStream oos = new ObjectOutputStream(
					new FileOutputStream(filename))){
				
				oos.writeObject(Car);
				
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}*/
		
	//write map <String,String>
	public static Object readObjectMap2(String filename) {
		try(ObjectInputStream ois = new ObjectInputStream(
				new FileInputStream(filename))){
			@SuppressWarnings("unchecked")
			HashMap<String,String> obj = (HashMap<String,String>)ois.readObject();
			System.out.println(obj);
			return obj;
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	public static void writeObjectMap2(String filename, HashMap<String,String> o) {
		try(ObjectOutputStream oos = new ObjectOutputStream(
				new FileOutputStream(filename))){
			
			oos.writeObject(o);
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
