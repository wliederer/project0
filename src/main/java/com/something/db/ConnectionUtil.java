package com.something.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;



public class ConnectionUtil {
	public static Connection connect() {
		try{
			Connection conn = DriverManager.getConnection(
		
				Config.getUrl(),
				Config.getUsername(),
				Config.getPassword());
		
			return conn;
		}catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
}
