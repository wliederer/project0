--Project0
--view tables
select * from users;
select * from passwordledger;
select * from cars;
select * from garages;
select * from payment;
select * from offers;

select * from users where title='Customer';
select * from users where title='Employee';

--delete from users where id='6';



--user defined function get max id from cars table
create or replace function get_largest() returns integer as $$
declare	
	largest integer;
begin
	select max("carid") into largest from "cars";
	
	return largest;
end; $$
language plpgsql;

select get_largest();

create table users(
	id integer unique primary key,
	firstname text,
	lastname text,
	title text,
	username text unique,
	"password" text
);
--drop table users;
select * from users;


--insert into users values(1,'will','liederer','Employee','bill','bill');
--insert into users values(2,'oprah','winfrey','Customer','oprah','oprah');


create table passwordledger(
	username text unique,
	"password" text,
	constraint FK_username foreign key (username) references users(username) on delete cascade
);



select * from passwordledger;

--drop table passwordledger;
insert into PasswordLedger select username, "password" from Employee;


create table cars(
	carid integer unique primary key,
	make text,
	model text,
	year integer,
	price float,
	salestatus text
);

--drop table cars;

select count(*) as NumCars from cars;

--insert into cars(make,model,year,price,salestatus) values('Astro','van',1989,4596.80,'pending');
--insert into cars values(1,'Astro','van',1989,4596.80,'pending');
--insert into cars values(2,'subaru','forester',2015,19876,'pending');

select * from cars;

create table offers(
	carid integer,
	customer integer,
	offer float,
	constraint FK_customerid foreign key (customer) references users(id) on delete cascade

	);

select * from offers;
--drop table offers;

create table garages(
	carid integer,
	customer integer,
	make text,
	model text,
	year integer,
	constraint FK_garageid foreign key (customer) references users(id) on delete cascade
	
);
--insert into garages values(1,6,'ferarri');
--drop table garages;
select * from garages;

create table payment(
	customer integer,
	payment float,
	numof integer,
	constraint FK_paymentid foreign key (customer) references users(id) on delete cascade
);
--drop table payment;
select * from payment;

insert into payment values('2','673.0','12');
